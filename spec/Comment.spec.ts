import server from '@server';
import supertest from 'supertest';

import { Response, SuperTest, Test } from 'supertest';
import {CREATED, OK} from 'http-status-codes';
import {SequelizeConnection} from '../src/SequelizeConnection';
import {CommentDto} from "@dtos";
import {CommentModel} from "@models";

describe('Comment', () => {

    let agent: SuperTest<Test>;

    // Comment added in startup in database
    const firstComment: CommentDto = {
        id: '4fb65bf8-bce8-4402-b1db-0007a43f52e9',
        message: 'Test message',
        postId: '808d6a18-29d9-447e-bb1d-65c338962cf3',
        authorId: '22470205-ad46-43f4-abee-9df20b9b2333',
    };

    // Comment to create
    const secondComment: CommentDto = {
        message: 'Test message 2',
        postId: '808d6a18-29d9-447e-bb1d-65c338962cf3',
        authorId: '22470205-ad46-43f4-abee-9df20b9b2333',
    };

    beforeAll(async (done) => {
        await SequelizeConnection.getInstance().sync();

        // Remove all and re-add
        CommentModel.destroy({
            where: {},
        }).then(() => {
            const commentModel = new CommentModel(firstComment);

            commentModel.save().then((createdComment) => {

                firstComment.createdAt = createdComment.createdAt;
                firstComment.updatedAt = createdComment.updatedAt;

                agent = supertest.agent(server.build());
                done();

            }).catch(console.log);
        }).catch(console.log);
    });

    it('Get all comments', (done) => {
        return agent.get(`/posts/${firstComment.postId}/comments`)
            .expect(OK)
            .end((err: Error, res: Response) => {
                expect(res.body.length).toBeGreaterThan(0);
                done();
            });
    });

    it('Get one comment', (done) => {
        return agent.get(`/posts/${firstComment.postId}/comments/${firstComment.id}`)
            .expect(OK)
            .end((err: Error, res: Response) => {
                expect(res.body.id).toBe(firstComment.id);
                done();
            });
    });

    it('Add a new comment', (done) => {
        return agent.post(`/posts/${secondComment.postId}/comments/`)
            .type('json')
            .send({
                message: secondComment.message,
                postId: secondComment.postId
            })
            .expect(CREATED)
            .end((err: Error, res: Response) => {
                if (res.header.location) {
                    const location = String(res.header.location).split('/');
                    secondComment.id = location[location.length - 1];
                }
                done();
            });
    });

    it('Edit a comment', () => {
        return agent.patch(`/posts/${firstComment.postId}/comments/${secondComment.id}`)
            .type('json')
            .send({
                message: 'Edited message',
            })
            .expect(OK);
    });

    it('Delete a comment', () => {
        return agent.delete(`/posts/${firstComment.postId}/comments/${secondComment.id}`)
            .expect(OK);
    });

});
