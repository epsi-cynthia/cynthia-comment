import {CommentMapper} from '@mappers';
import {CommentModel} from '@models';
import {CommentDto} from "@dtos";
import {SequelizeConnection} from "../src/SequelizeConnection";

describe('Mapper', () => {

    const uuid = '7de29744-872a-4396-bd03-27a0409779c7';

    it('Map CommentModel to CommentDto', async (done) => {
        await SequelizeConnection.getInstance().sync();

        // Unit test
        const commentModel = new CommentModel({
            id: uuid,
            message: 'test',
            postId: uuid,
            authorId: uuid,
        });
        const commentDto = new CommentMapper().mapToDto(commentModel);

        expect(commentDto).toEqual(new CommentDto('test', uuid, uuid, uuid));
        done();
    });

});
