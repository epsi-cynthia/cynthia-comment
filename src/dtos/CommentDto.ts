import {ApiModel, ApiModelProperty, SwaggerDefinitionConstant} from 'swagger-express-ts';

@ApiModel({
    name: 'Comment',
})
export class CommentDto {

    @ApiModelProperty({
        description: 'Id of comment',
    })
    public id?: string;

    @ApiModelProperty({
        description: 'Message of comment',
        required: true,
    })
    public message: string;

    @ApiModelProperty({
        description: 'Post attached to comment',
        required: true,
    })
    public postId: string;

    @ApiModelProperty({
        description: 'User attached to comment',
        required: true,
    })
    public authorId: string;

    @ApiModelProperty({
        description: 'Created date of comment',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public createdAt?: Date;

    @ApiModelProperty({
        description: 'Updated date of comment',
        type: SwaggerDefinitionConstant.STRING,
        format: 'date',
    })
    public updatedAt?: Date;

    constructor(message: string, postId: string, authorId: string, id?: string) {
        this.id = id;
        this.message = message;
        this.postId = postId;
        this.authorId = authorId;
    }

}
