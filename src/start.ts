import server from '@server';
import { logger } from '@shared';
import {SequelizeConnection} from './SequelizeConnection';

(async () => {
    await SequelizeConnection.getInstance().sync();

    // Start the server
    const port = Number(process.env.PORT || 3000);
    const app = server.build();
    app.listen(port, () => {
        logger.info('Cynthia Comment started on port: ' + port);
    });
})();
