import {CommentDto} from '@dtos';
import {CommentModel} from '@models';
import {ErrorCode, logger} from '@shared';
import {CommentMapper} from '@mappers';
import {inject, injectable} from "inversify";

@injectable()
export class CommentService {

    constructor(@inject(CommentMapper.name) private commentMapper: CommentMapper) {
    }
    
    public getAllComments(postId: string): Promise<CommentDto[]> {
        return new Promise((resolve, reject) => {
            CommentModel.findAll({
                where: { postId }
            }).then((comments) => resolve(
                comments.map((comment) => this.commentMapper.mapToDto(comment))
            )).catch((err) => {
                logger.error(err.stack);
                return reject(ErrorCode.INTERNAL_ERROR);
            });
        });
    }

    public getCommentById(id: string): Promise<CommentDto> {
        return new Promise((resolve, reject) => {
            CommentModel.findByPk(id).then((comment) => {
                if (!comment) {
                    return reject(ErrorCode.NO_COMMENT);
                }

                return resolve(this.commentMapper.mapToDto(comment));
            }).catch((err) => {
                logger.error(err.stack);
                return reject(ErrorCode.INTERNAL_ERROR);
            });

        });
    }

    public addComment(userId: string, postId: string, comment: CommentDto): Promise<CommentDto> {
        return new Promise((resolve, reject) => {
            comment.authorId = userId;
            comment.postId = postId;
            const commentModel = new CommentModel(comment);

            commentModel.save()
                .then((commentCreated) => resolve(this.commentMapper.mapToDto(commentCreated)))
                .catch((err) => {
                    logger.error(err.stack);
                    return reject(ErrorCode.INTERNAL_ERROR);
                });
        });
    }

    public editCommentById(id: string, userId: string, message: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            CommentModel.findByPk(id).then((comment) => {
                if (!comment) { return reject(ErrorCode.NO_COMMENT); }

                if (comment.authorId != userId) { return reject(ErrorCode.DONT_RIGHT_TO_EDIT); }

                CommentModel.update({ message }, {
                    where: { id }
                }).then((res) => {
                    if (res[0] === 0) { return reject(ErrorCode.NO_COMMENT); }

                    return resolve(true);
                }).catch((err) => {
                    logger.error(err.stack);
                    return reject(ErrorCode.INTERNAL_ERROR);
                });

            }).catch((err) => {
                logger.error(err.stack);
                return reject(ErrorCode.INTERNAL_ERROR);
            });
        });
    }

    public deleteCommentById(id: string, userId: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            CommentModel.findByPk(id).then((comment) => {
                if (!comment) {
                    return reject(ErrorCode.NO_COMMENT);
                }

                if (comment.authorId != userId) {
                    return reject(ErrorCode.DONT_RIGHT_TO_EDIT);
                }
                CommentModel.destroy({
                    where: {
                        id,
                    },
                }).then((res) => {
                    if (!res) {
                        return reject(ErrorCode.NO_COMMENT);
                    }

                    return resolve(true);
                }).catch(() => reject(ErrorCode.INTERNAL_ERROR));

            }).catch((err) => {
                logger.error(err.stack);
                return reject(ErrorCode.INTERNAL_ERROR);
            });
        });
    }

}
