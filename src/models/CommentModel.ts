import {Column, DataType, Default, Length, Model, PrimaryKey, Table} from 'sequelize-typescript';
import {mapTo} from '@wufe/mapper';
import {CommentDto} from '@dtos';

@mapTo(CommentDto)
@Table({
    tableName: 'comment',
})
export class CommentModel extends Model<CommentModel> {

    @Default(DataType.UUIDV4)
    @PrimaryKey
    @Column(DataType.UUID)
    public id!: string;

    @Length({max: 255})
    @Column
    public message!: string;

    @Column(DataType.UUID)
    public postId!: string;

    @Column(DataType.UUID)
    public authorId!: string;

}
