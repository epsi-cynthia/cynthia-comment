import cookieParser from 'cookie-parser';
import express from 'express';
import morgan from 'morgan';
import * as swagger from 'swagger-express-ts';
import {SwaggerService} from 'swagger-express-ts/swagger.service';
import swaggerUi from 'swagger-ui-express';
import {interfaces, InversifyExpressServer, TYPE} from 'inversify-express-utils';
import {Container} from 'inversify';
import {CommentCtrl, HealthCtrl} from '@controllers';
import {CommentMapper} from '@mappers';
import {CommentService, ControllerHelper} from '@services';
import {ErrorCode, KeycloakAuth, logger} from '@shared';
import session from 'express-session';
import cors from 'cors';

// Set up container
const container = new Container();

container
    .bind<ControllerHelper>(ControllerHelper.name)
    .to(ControllerHelper)
    .inSingletonScope();
container
    .bind<CommentMapper>(CommentMapper.name)
    .to(CommentMapper)
    .inSingletonScope();
container
    .bind<CommentService>(CommentService.name)
    .to(CommentService);
container
    .bind<interfaces.Controller>(TYPE.Controller)
    .to(HealthCtrl)
    .whenTargetNamed('Health');
container
    .bind<interfaces.Controller>(TYPE.Controller)
    .to(CommentCtrl)
    .whenTargetNamed('Comment');

// Init server
const server = new InversifyExpressServer(container);

server.setConfig((app) => {

    // Add middleware/settings/routes to express.
    app.use(morgan('dev'));
    app.use(express.json());
    app.use(express.urlencoded({extended: true}));
    app.use(cookieParser());

    // Setup CORS
    app.use(cors());

    // Create session
    app.use(session({
        secret: process.env.KC_SESSION_SECRET_KEY || '',
        resave: false,
        saveUninitialized: true,
        store: new session.MemoryStore()
    }));

    // Keycloak authentication
    app.use(KeycloakAuth.getInstance().middleware({
        logout: '/logout',
    }));


    // Generate swagger docs
    app.use( swagger.express({
        definition : {
            info : {
                title : 'Cynthia Comment' ,
                version : '1.0',
            },
        },
    }));

    // Display swagger ui
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(
        SwaggerService.getInstance().getData(),
    ));

});

server.setErrorConfig((app: any) => {
    app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
        container.get<ControllerHelper>(ControllerHelper.name).handleError(req, res, ErrorCode.NOT_FOUND);
    });

    app.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
        logger.error(err);

        const errorCode = err.message.includes('keycloak-token')
            ? ErrorCode.BAD_FORMAT_TOKEN
            : ErrorCode.INTERNAL_ERROR;

        container.get<ControllerHelper>(ControllerHelper.name).handleError(req, res, errorCode);
    });
});

// Export express instance
export default server;
