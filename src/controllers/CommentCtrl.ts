import * as express from 'express';
import {
    ApiOperationDelete,
    ApiOperationGet,
    ApiOperationPatch,
    ApiOperationPost,
    ApiPath,
    SwaggerDefinitionConstant,
} from 'swagger-express-ts';
import {CommentService, ControllerHelper} from '@services';
import {CommentDto} from '@dtos';
import {ENV, IErrorCode, KeycloakAuth} from '@shared';
import {CREATED, OK} from 'http-status-codes';
import {
    controller,
    httpDelete,
    httpGet,
    httpPatch,
    httpPost,
    interfaces,
    requestBody,
    requestParam
} from "inversify-express-utils";
import {body, param} from "express-validator";
import {inject} from "inversify";
import Controller = interfaces.Controller;

@ApiPath({
    path: '/posts/{postId}/comments',
    name: 'Comment',
})
@controller(
    '/posts/:postId/comments',
    !(process.env.NODE_ENV || '').match(ENV.test) ? KeycloakAuth.getInstance().protect() : KeycloakAuth.getInstance().middleware()
)
export class CommentCtrl implements Controller {

    public static readonly ERROR_DEFINE_ID_IS_UNAUTHORIZED = 'Vous ne devez pas indiquer l\'identifiant du commentaire';
    public static readonly ERROR_MESSAGE_REQUIRED_BETWEEN_1_AND_255 = 'Le message est obligatoire et doit contenir 255 caractères maximum.';
    public static readonly ERROR_BAD_ID = 'L\'identifiant n\'est pas valide.';
    public static readonly ERROR_ID_MISSING = 'L\'identifiant :field est manquant.';

    constructor(
        @inject(CommentService.name) private commentService: CommentService,
        @inject(ControllerHelper.name) private controllerHelper: ControllerHelper,
    ) {}

    @ApiOperationGet({
        summary: 'Get all comments',
        description: 'Get comments of the post',
        responses: {
            200: {
                description: 'Success',
                type: SwaggerDefinitionConstant.ARRAY,
                model: 'Comment',
            },
        },
    })
    @httpGet('')
    public getAllComments(@requestParam('postId') postId: string, req: express.Request, res: express.Response) {
        return this.commentService.getAllComments(req.params.postId)
            .then((comments: CommentDto[]) => res.status(OK).json(comments))
            .catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

    @ApiOperationGet({
        path: '/{commentId}',
        summary: 'Get comment by id',
        description: 'Get a comment with its id',
        parameters: {
            path: {
                postId: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
                commentId: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: {
                description: 'Success',
                type: SwaggerDefinitionConstant.OBJECT,
                model: 'Comment',
            },
        },
    })
    @httpGet('/:commentId')
    public getCommentById(@requestParam('commentId') id: string, req: express.Request, res: express.Response) {
        return this.commentService.getCommentById(id).then((comment: CommentDto) => {
            res.status(OK).json(comment);
        }).catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

    @ApiOperationPost({
        summary: 'Add a new comment',
        description: 'Add a new comment to the post',
        parameters: {
            body: {
                properties: {
                    message: {
                        type: SwaggerDefinitionConstant.STRING,
                        required: true,
                    },
                },
            },
        },
        responses: {
            201: { description: 'Created' },
        },
    })
    @httpPost('',
        param('postId').isUUID(4).withMessage( CommentCtrl.ERROR_ID_MISSING.replace(':field', 'du post') ),
        body('id').not().exists().withMessage(CommentCtrl.ERROR_DEFINE_ID_IS_UNAUTHORIZED),
        body('message').isLength({ min: 1, max: 255 }).withMessage(CommentCtrl.ERROR_MESSAGE_REQUIRED_BETWEEN_1_AND_255),
    )
    public addComment(
        @requestParam('postId') postId: string, @requestBody() commentDto: CommentDto,
        req: express.Request, res: express.Response,
    ) {
        if (this.controllerHelper.errorWithParams(req, res)) { return res; }

        const userId = (req as any).kauth.grant.access_token.content.sub;
        return this.commentService.addComment(userId, postId, commentDto)
            .then((comment: CommentDto) =>
                res.header('location', `/posts/${comment.postId}/comments/${comment.id}`).sendStatus(CREATED)
            )
            .catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

    @ApiOperationPatch({
        path: '/{commentId}',
        summary: 'Edit a comment',
        description: 'Edit a comment of the post',
        parameters: {
            path: {
                postId: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
                commentId: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
            body: {
                properties: {
                    message: {
                        type: SwaggerDefinitionConstant.STRING,
                        required: true,
                    },
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpPatch('/:commentId',
        param('postId').isUUID(4).withMessage( CommentCtrl.ERROR_ID_MISSING.replace(':field', 'du post') ),
        param('commentId').isUUID(4).withMessage(CommentCtrl.ERROR_BAD_ID),
        body('message').isLength({ min: 1, max: 255 }).withMessage(CommentCtrl.ERROR_MESSAGE_REQUIRED_BETWEEN_1_AND_255),
    )
    public editCommentById(
        @requestParam('commentId') id: string, @requestBody() commentDto: CommentDto,
        req: express.Request, res: express.Response
    ) {
        if (this.controllerHelper.errorWithParams(req, res)) { return res; }

        const userId = (req as any).kauth.grant.access_token.content.sub;
        return this.commentService.editCommentById(id, userId, commentDto.message)
            .then(() => res.sendStatus(OK))
            .catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

    @ApiOperationDelete({
        path: '/{commentId}',
        summary: 'Delete a comment',
        description: 'Delete a comment of the post',
        parameters: {
            path: {
                postId: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
                commentId: {
                    type: SwaggerDefinitionConstant.STRING,
                    required: true,
                },
            },
        },
        responses: {
            200: { description: 'Success' },
        },
    })
    @httpDelete('/:commentId',
        param('commentId').isUUID(4).withMessage(CommentCtrl.ERROR_BAD_ID),
    )
    public deleteCommentById(@requestParam('commentId') id: string, req: express.Request, res: express.Response) {
        if (this.controllerHelper.errorWithParams(req, res)) { return res; }

        const userId = (req as any).kauth.grant.access_token.content.sub;
        return this.commentService.deleteCommentById(id, userId).then(() =>
            res.sendStatus(OK)
        ).catch((err: IErrorCode) => this.controllerHelper.handleError(req, res, err));
    }

}
