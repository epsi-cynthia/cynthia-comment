import * as express from 'express';
import {ApiOperationGet, ApiPath} from 'swagger-express-ts';
import {controller, httpGet, interfaces} from "inversify-express-utils";
import Controller = interfaces.Controller;

@ApiPath({
    path: '/health',
    name: 'Health',
})
@controller('/health')
export class HealthCtrl implements Controller {

    @ApiOperationGet({
        summary: 'Display health of the service',
        responses: {
            200: { description: 'Service is up' },
        },
    })
    @httpGet('')
    public health(req: express.Request, res: express.Response) {
        res.json({
            status: 'up',
            uptime: process.uptime(),
        });
    }

}
