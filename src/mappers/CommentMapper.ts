import {Mapper} from '@wufe/mapper';
import {CommentModel} from '@models';
import {CommentDto} from '@dtos';
import {injectable} from "inversify";

@injectable()
export class CommentMapper {

    private mapper?: Mapper;

    public mapToDto(comment: CommentModel): CommentDto {
        return this.initializeMapper().map<CommentModel, CommentDto>(comment);
    }

    private initializeMapper(): Mapper {
        if (!this.mapper) {
            this.mapper = new Mapper()
                .withConfiguration((configuration) => configuration.shouldIgnoreSourcePropertiesIfNotInDestination(true));

            this.mapper.createMap<CommentModel, CommentDto>(CommentModel)
                .forMember('id', (opt) => opt.mapFrom((src) => src.id))
                .forMember('message', (opt) => opt.mapFrom((src) => src.message))
                .forMember('postId', (opt) => opt.mapFrom((src) => src.postId))
                .forMember('authorId', (opt) => opt.mapFrom((src) => src.authorId))
                .forMember('createdAt', (opt) => opt.mapFrom((src) => src.createdAt))
                .forMember('updatedAt', (opt) => opt.mapFrom((src) => src.updatedAt));
        }

        return this.mapper;
    }

}
